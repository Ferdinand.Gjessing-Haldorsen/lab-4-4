package no.uib.inf101.gridview;
import java.awt.Color;
import javax.swing.JFrame;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.ColorGrid;
import no.uib.inf101.colorgrid.IColorGrid;


public class Main {
  public static void main(String[] args) {

        // Opprett et ColorGrid-objekt med 3 rader og 4 kolonner
        IColorGrid colorGrid = new ColorGrid(3, 4);
        
        // Sette fargene i hjørnene
        colorGrid.set(new CellPosition(0, 0), Color.RED);    // Øvre venstre hjørne
        colorGrid.set(new CellPosition(0, 3), Color.BLUE);   // Øvre høyre hjørne
        colorGrid.set(new CellPosition(2, 0), Color.YELLOW); // Nedre venstre hjørne
        colorGrid.set(new CellPosition(2, 3), Color.GREEN);  // Nedre høyre hjørne

        // Opprett GridView med colorGrid
        GridView canvas = new GridView(colorGrid);

        // Opprett JFrame og konfigurer den
        JFrame frame = new JFrame("INF101");
        frame.add(canvas); 
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack(); 
        frame.setVisible(true);
    }
}