package no.uib.inf101.colorgrid;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ColorGrid implements IColorGrid {

  // TODO: Implement this class
  public int rows;
  public int cols;
  private Color[][] grid; // 2D array to store cell colors
  CellPosition cellPosition = null;


  public ColorGrid (int rows, int cols) {
    this.rows = rows;
    this.cols = cols;
    this.grid = new Color[rows][cols];
  }


  @Override
  public Color get(CellPosition pos) {
    if (pos.row() >= 0 && pos.row() < rows && pos.col() >= 0 && pos.col() < cols) {
      return grid[pos.row()][pos.col()]; // Return the color at the specified position
  } else {
      throw new IndexOutOfBoundsException("Position is out of bounds");
  }
}

  
  @Override
  public void set(CellPosition pos, Color color) {
    if (pos.row() >= 0 && pos.row() < rows && pos.col() >= 0 && pos.col() < cols) {
      grid[pos.row()][pos.col()] = color; // Assign the color to the cell at the specified position
  } else {
      throw new IndexOutOfBoundsException("Position is out of bounds");
  }}

  @Override
  public int rows(){
    return rows;
  }

  @Override
  public int cols(){
    return cols;
  }
  
  @Override
  public List<CellColor> getCells(){
    List<CellColor> cellColors = new ArrayList<>(); // Correctly initialize the list
    for (int row = 0; row < rows; row++) {
        for (int col = 0; col < cols; col++) {
            CellPosition position = new CellPosition(row, col);
            Color color = grid[row][col]; // This assumes your grid is initialized and possibly modified
            cellColors.add(new CellColor(position, color)); // Add CellColor object to list
        }
    }
    return cellColors;
}

}
